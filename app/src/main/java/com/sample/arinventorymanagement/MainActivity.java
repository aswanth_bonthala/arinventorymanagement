package com.sample.arinventorymanagement;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;

import com.sample.arinventorymanagement.admin.AdminLoginActivity;
import com.sample.arinventorymanagement.user.UserDashboardActivity;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView user, admin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        admin = findViewById(R.id.iv_admin);
        user = findViewById(R.id.iv_user);

        user.setOnClickListener(this);
        admin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_admin:
                startActivity(new Intent(MainActivity.this, AdminLoginActivity.class));
                break;

            case R.id.iv_user:
                startActivity(new Intent(MainActivity.this, UserDashboardActivity.class));
                break;

            default:
                break;
        }

    }
}
