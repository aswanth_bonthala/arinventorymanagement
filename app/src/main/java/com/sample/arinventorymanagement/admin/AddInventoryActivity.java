package com.sample.arinventorymanagement.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.dpizarro.uipicker.library.picker.PickerUI;
import com.dpizarro.uipicker.library.picker.PickerUISettings;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sample.arinventorymanagement.CommonData;
import com.sample.arinventorymanagement.MainActivity;
import com.sample.arinventorymanagement.R;
import com.sample.arinventorymanagement.admin.model.ProductDetails;
import com.sample.arinventorymanagement.custom.AwesomeProgressDialog;
import com.sample.arinventorymanagement.custom.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class AddInventoryActivity extends AppCompatActivity {

    TextInputEditText mName, mBrand, mType, mCategory, mSize, mPrice, mDescription;
    PickerUI mBrandPickerUi, mCategoryPickerUi, mTypePickerUi;
    ImageView logout;

    List<String> brandList;
    List<String> typeList;
    List<String> categoryList;
    Button mProductRegister;

    //Firebase Database references
    DatabaseReference myRef;
    FirebaseDatabase database;

    private Dialog awesomeProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_inventory);

        //UI Initialize
        mName = findViewById(R.id.product_name_et);
        mBrand = findViewById(R.id.product_brand_et);
        mType = findViewById(R.id.product_type_et);
        mCategory = findViewById(R.id.product_category_et);
        mSize = findViewById(R.id.product_size_et);
        mPrice = findViewById(R.id.product_price_et);
        mDescription = findViewById(R.id.product_description_et);
        mProductRegister = findViewById(R.id.add_inventory);
        logout = findViewById(R.id.logout);


        mBrandPickerUi = findViewById(R.id.brand_picker_ui_view);
        mTypePickerUi = findViewById(R.id.type_picker_ui_view);
        mCategoryPickerUi = findViewById(R.id.category_picker_ui_view);


        brandList = Arrays.asList(getResources().getStringArray(R.array.product_brand_list));
        typeList = Arrays.asList(getResources().getStringArray(R.array.product_type_list));
        categoryList = Arrays.asList(getResources().getStringArray(R.array.product_category_list));


        mBrand.setOnClickListener(v -> {

            mBrandPickerUi.setVisibility(View.VISIBLE);
            PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                    .withItems(brandList)
                    .withAutoDismiss(true)
                    .withItemsClickables(false)
                    .withUseBlur(false)
                    .build();

            mBrandPickerUi.setSettings(pickerUISettings);
            mBrandPickerUi.slide();
        });

        logout.setOnClickListener(view -> {
            showLogoutAlert();
        });
        mType.setOnClickListener(v -> {

            mTypePickerUi.setVisibility(View.VISIBLE);
            PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                    .withItems(typeList)
                    .withAutoDismiss(true)
                    .withItemsClickables(false)
                    .withUseBlur(false)
                    .build();

            mTypePickerUi.setSettings(pickerUISettings);
            mTypePickerUi.slide();
        });

        mCategory.setOnClickListener(v -> {

            mCategoryPickerUi.setVisibility(View.VISIBLE);
            PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                    .withItems(categoryList)
                    .withAutoDismiss(true)
                    .withItemsClickables(false)
                    .withUseBlur(false)
                    .build();

            mCategoryPickerUi.setSettings(pickerUISettings);
            mCategoryPickerUi.slide();
        });

        mBrandPickerUi.setOnClickItemPickerUIListener((which, position, valueResult) -> {
            Toast.makeText(AddInventoryActivity.this, valueResult, Toast.LENGTH_SHORT).show();
            mBrand.setText(valueResult);
            mBrand.setError(null);
        });

        mCategoryPickerUi.setOnClickItemPickerUIListener((which, position, valueResult) -> {
            Toast.makeText(AddInventoryActivity.this, valueResult, Toast.LENGTH_SHORT).show();
            mCategory.setText(valueResult);
            mCategory.setError(null);
        });

        mTypePickerUi.setOnClickItemPickerUIListener((which, position, valueResult) -> {
            Toast.makeText(AddInventoryActivity.this, valueResult, Toast.LENGTH_SHORT).show();
            mType.setText(valueResult);
            mType.setError(null);
        });


        mProductRegister.setOnClickListener(v -> {

            if(validateFields())
            {
                String mProductId = UUID.randomUUID().toString();

                myRef =  fireBaseInitialSetup();

                final ProductDetails productDetails = new ProductDetails();
                productDetails.setmName(mName.getText().toString());
                productDetails.setmType(mType.getText().toString());
                productDetails.setmBrand(mBrand.getText().toString());
                productDetails.setmCategory(mCategory.getText().toString());
                productDetails.setmSize(mSize.getText().toString());
                productDetails.setmPrice(mPrice.getText().toString());
                productDetails.setmDescription(mDescription.getText().toString());

                try {
                    awesomeProgressDialog = new AwesomeProgressDialog(AddInventoryActivity.this).setMessage(R.string.adding_product).show();

                    myRef.child(CommonData.PRODUCTS).child(mProductId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if(dataSnapshot.exists())
                            {

                                myRef.child(mProductId).setValue(productDetails).addOnCompleteListener(task -> {

                                    if(task.isSuccessful()){
                                        // write code for successfull operation
                                        awesomeProgressDialog.dismiss();
                                        // Toast.makeText(AddExpensesActivity.this, "Bill Receipt for "+sharedPreferences.getString(CommonData.DISPLAY_NAME,"") + " is stored successfully", Toast.LENGTH_SHORT).show();
                                        Utils.showSuccessDialogue(AddInventoryActivity.this,"Product "+mName.getText().toString()+ " is added successfully",AddInventoryActivity.class,true);

                                    }
                                });
                            }
                            else
                            {
                                myRef.child(mProductId).setValue(productDetails).addOnCompleteListener(task -> {

                                    if(task.isSuccessful()){
                                        // write code for successfull operation
                                        awesomeProgressDialog.dismiss();
                                        // Toast.makeText(AddExpensesActivity.this, "Bill Receipt for "+sharedPreferences.getString(CommonData.DISPLAY_NAME,"") + " is stored successfully", Toast.LENGTH_SHORT).show();
                                        Utils.showSuccessDialogue(AddInventoryActivity.this,"Product "+mName.getText().toString()+ " is added successfully",AddInventoryActivity.class,true);

                                    }
                                });
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            awesomeProgressDialog.dismiss();
                            Toast.makeText(AddInventoryActivity.this, "Something went wrong. Please try again..!!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    awesomeProgressDialog.dismiss();
                    Toast.makeText(AddInventoryActivity.this, "Unable to process request. Please try again..!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private DatabaseReference fireBaseInitialSetup() {

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference(CommonData.PRODUCTS);
        return myRef;
    }

    private void showLogoutAlert() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddInventoryActivity.this);
        alertDialog.setTitle(getResources().getString(R.string.logout));
        alertDialog.setMessage(getResources().getString(R.string.are_you_sure_logout));
        alertDialog.setCancelable(false);

        alertDialog.setPositiveButton(getResources().getString(R.string.logout), (dialog, which) -> {
            dialog.cancel();

            Toast.makeText(getApplicationContext(), getResources().getString(R.string.logout_success), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(AddInventoryActivity.this, MainActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        });

        alertDialog.setNegativeButton(getResources().getString(R.string.cancel), (dialog, which) -> dialog.cancel());
        alertDialog.show();

    }

    private boolean validateFields() {

        if (mName.getText().toString().trim().length() == 0) {
            setFocus(mName, getResources().getString(R.string.enter_name));
            return false;
        }
        if (mType.getText().toString().trim().length() == 0) {
            setFocus(mType, getResources().getString(R.string.select_type));
            return false;
        }
        if (mCategory.getText().toString().trim().length() == 0) {
            setFocus(mCategory, getResources().getString(R.string.select_category));
            return false;
        }if (mBrand.getText().toString().trim().length() == 0) {
            setFocus(mBrand, getResources().getString(R.string.select_brand));
            return false;
        }if (mSize.getText().toString().trim().length() == 0) {
            setFocus(mSize, getResources().getString(R.string.enter_size));
            return false;
        }
        if (mPrice.getText().toString().trim().length() == 0) {
            setFocus(mPrice, getResources().getString(R.string.enter_price));
            return false;
        }
        if (mDescription.getText().toString().trim().length() == 0) {
            setFocus(mDescription, getResources().getString(R.string.enter_description));
            return false;
        }

        return true;
    }

    private void setFocus(TextInputEditText editText, String string) {
        editText.requestFocus();
        (editText).setError(Html.fromHtml("<font color='red'>" + string + "</font>"));
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        showLogoutAlert();
    }
}
