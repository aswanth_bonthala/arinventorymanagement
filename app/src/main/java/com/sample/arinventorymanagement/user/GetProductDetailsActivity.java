package com.sample.arinventorymanagement.user;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sample.arinventorymanagement.CommonData;
import com.sample.arinventorymanagement.R;
import com.sample.arinventorymanagement.admin.model.ProductDetails;
import com.sample.arinventorymanagement.custom.AwesomeProgressDialog;
import com.sample.arinventorymanagement.user.adapter.ProductDatilsAdapter;
import com.sample.arinventorymanagement.user.adapter.ProjectDatilsAdapter;
import com.sample.arinventorymanagement.user.model.RoomDetails;

import java.util.ArrayList;

public class GetProductDetailsActivity extends AppCompatActivity {
    RecyclerView productsList;
    TextView tvNoProjects;
    //Firebase Database references
    DatabaseReference myRef;
    FirebaseDatabase database;
    private Dialog awesomeProgressDialog;
    ArrayList<ProductDetails> productDetailsArrayList = new ArrayList<ProductDetails>();
    ProductDatilsAdapter productDatilsAdapter;
    String mProductType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_product_details);
        productsList = findViewById(R.id.products_list);
        tvNoProjects = findViewById(R.id.tv_no_projects_available);
        mProductType = getIntent().getStringExtra("productType");
        myRef = fireBaseInitialSetup();
        fetchProductDetails();


    }

    private void fetchProductDetails() {

        awesomeProgressDialog = new AwesomeProgressDialog(GetProductDetailsActivity.this).setMessage(R.string.load_details).show();
        productDetailsArrayList.clear();
        try {
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (dataSnapshot.exists()) {
                        awesomeProgressDialog.dismiss();
                        tvNoProjects.setVisibility(View.GONE);
                        productsList.setVisibility(View.VISIBLE);
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            ProductDetails productDetails = dataSnapshot1.getValue(ProductDetails.class);
                            productDetailsArrayList.add(productDetails);
                        }
                        productDatilsAdapter = new ProductDatilsAdapter(GetProductDetailsActivity.this, productDetailsArrayList);
                        awesomeProgressDialog.dismiss();
                        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(GetProductDetailsActivity.this);
                        productsList.setLayoutManager(linearLayoutManager);
                        productsList.setItemAnimator(new DefaultItemAnimator());
                        productsList.setAdapter(productDatilsAdapter);
                        productDatilsAdapter.filter(mProductType);
                        productDatilsAdapter.notifyDataSetChanged();

                    } else {
                        awesomeProgressDialog.dismiss();
                        tvNoProjects.setVisibility(View.VISIBLE);
                        productsList.setVisibility(View.GONE);
                        Toast.makeText(GetProductDetailsActivity.this, "Product details were not found", Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    awesomeProgressDialog.dismiss();
                    tvNoProjects.setVisibility(View.VISIBLE);
                    productsList.setVisibility(View.GONE);
                    Toast.makeText(GetProductDetailsActivity.this, "No Results Found", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            awesomeProgressDialog.dismiss();
            tvNoProjects.setVisibility(View.VISIBLE);
            productsList.setVisibility(View.GONE);
            Toast.makeText(GetProductDetailsActivity.this, "Unable to process request. Please try again..!!", Toast.LENGTH_SHORT).show();
        }
    }

    private DatabaseReference fireBaseInitialSetup() {

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference(CommonData.PRODUCTS);
        return myRef;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (awesomeProgressDialog != null)
            awesomeProgressDialog.dismiss();
    }
}
