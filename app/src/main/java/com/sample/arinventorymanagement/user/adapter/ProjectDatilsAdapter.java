package com.sample.arinventorymanagement.user.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.arinventorymanagement.R;
import com.sample.arinventorymanagement.admin.model.ProductDetails;
import com.sample.arinventorymanagement.user.GetProductDetailsActivity;
import com.sample.arinventorymanagement.user.model.RoomDetails;

import java.util.ArrayList;
import java.util.Locale;

public class ProjectDatilsAdapter extends RecyclerView.Adapter<ProjectDatilsAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<RoomDetails> roomDetailsArrayList;
    private ArrayList<RoomDetails> filterList = new ArrayList<>();


    public ProjectDatilsAdapter(Context context, ArrayList<RoomDetails> roomDetailsArrayList) {
        this.context = context;
        this.roomDetailsArrayList = roomDetailsArrayList;
        this.filterList.addAll(this.roomDetailsArrayList);
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_projects_details, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.projectName.setText(filterList.get(position).getmProjectName());
        holder.projectType.setText(filterList.get(position).getmProjectType());
        holder.flatType.setText(filterList.get(position).getmFlatType());
        holder.flatSft.setText(filterList.get(position).getmFlatSft()+" Sft");
        holder.roomType.setText(filterList.get(position).getmRoomType());
        holder.roomMeasurement.setText(filterList.get(position).getmRoomMeasurement());

        holder.viewProducts.setOnClickListener(v -> {
            try {
                Intent intent = new Intent(context, GetProductDetailsActivity.class);
                intent.putExtra("productType",filterList.get(position).getmRoomType());
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != filterList ? filterList.size() : 0);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView projectName;
        TextView projectType;
        TextView flatType;
        TextView flatSft;
        TextView roomType;
        TextView roomMeasurement;
        Button viewProducts;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            projectName = itemView.findViewById(R.id.tv_project_name);
            projectType = itemView.findViewById(R.id.tv_project_type);
            flatType = itemView.findViewById(R.id.tv_flat_type);
            flatSft = itemView.findViewById(R.id.tv_flat_sft);
             roomType = itemView.findViewById(R.id.tv_room_type);
            roomMeasurement = itemView.findViewById(R.id.tv_room_sft);
            viewProducts = itemView.findViewById(R.id.bt_view_products);
        }
    }

    // Do Search...
    public void filter(final String text) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {

                // Clear the filter list
                filterList.clear();

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {

                    filterList.addAll(roomDetailsArrayList);

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (RoomDetails roomDetails : roomDetailsArrayList) {
                        if (roomDetails.getmRoomType().toLowerCase(Locale.getDefault()).contains(text.toLowerCase())) {
                            // Adding Matched items
                            filterList.add(roomDetails);
                        }
                    }
                }

                // Set on UI Thread
                ((Activity) context).runOnUiThread(() -> {
                    // Notify the List that the DataSet has changed...
                    notifyDataSetChanged();
                });

            }
        }).start();

    }
}
