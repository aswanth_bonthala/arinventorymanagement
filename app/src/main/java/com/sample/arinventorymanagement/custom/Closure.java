package com.sample.arinventorymanagement.custom;

/**
 * Created by blennersilva on 21/08/17.
 */

public interface Closure {
    void exec();
}
