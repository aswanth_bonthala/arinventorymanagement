package com.sample.arinventorymanagement.user.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RoomDetails implements Parcelable {
    private String mProjectName;
    private String mProjectType;
    private String mFlatType;
    private String mFlatSft;
    private String mRoomType;

    public RoomDetails() {
    }

    public String getmProjectName() {
        return mProjectName;
    }

    public void setmProjectName(String mProjectName) {
        this.mProjectName = mProjectName;
    }

    public String getmProjectType() {
        return mProjectType;
    }

    public void setmProjectType(String mProjectType) {
        this.mProjectType = mProjectType;
    }

    public String getmFlatType() {
        return mFlatType;
    }

    public void setmFlatType(String mFlatType) {
        this.mFlatType = mFlatType;
    }

    public String getmFlatSft() {
        return mFlatSft;
    }

    public void setmFlatSft(String mFlatSft) {
        this.mFlatSft = mFlatSft;
    }

    public String getmRoomType() {
        return mRoomType;
    }

    public void setmRoomType(String mRoomType) {
        this.mRoomType = mRoomType;
    }

    public String getmRoomMeasurement() {
        return mRoomMeasurement;
    }

    public void setmRoomMeasurement(String mRoomMeasurement) {
        this.mRoomMeasurement = mRoomMeasurement;
    }

    private String mRoomMeasurement;


    protected RoomDetails(Parcel in) {
        mProjectName = in.readString();
        mProjectType = in.readString();
        mFlatType = in.readString();
        mFlatSft = in.readString();
        mRoomType = in.readString();
        mRoomMeasurement = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mProjectName);
        dest.writeString(mProjectType);
        dest.writeString(mFlatType);
        dest.writeString(mFlatSft);
        dest.writeString(mRoomType);
        dest.writeString(mRoomMeasurement);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RoomDetails> CREATOR = new Creator<RoomDetails>() {
        @Override
        public RoomDetails createFromParcel(Parcel in) {
            return new RoomDetails(in);
        }

        @Override
        public RoomDetails[] newArray(int size) {
            return new RoomDetails[size];
        }
    };
}
