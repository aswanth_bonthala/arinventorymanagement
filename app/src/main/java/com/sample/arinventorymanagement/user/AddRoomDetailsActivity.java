package com.sample.arinventorymanagement.user;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.dpizarro.uipicker.library.picker.PickerUI;
import com.dpizarro.uipicker.library.picker.PickerUISettings;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sample.arinventorymanagement.CommonData;
import com.sample.arinventorymanagement.R;
import com.sample.arinventorymanagement.admin.AddInventoryActivity;
import com.sample.arinventorymanagement.admin.model.ProductDetails;
import com.sample.arinventorymanagement.custom.AwesomeProgressDialog;
import com.sample.arinventorymanagement.custom.Utils;
import com.sample.arinventorymanagement.user.model.RoomDetails;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class AddRoomDetailsActivity extends AppCompatActivity {

    Button btSubmit;
    PickerUI mProjectTypePickerUi, mFlatTypePickerUi, mRoomTypePickerUi;
    TextInputEditText mProjectName, mProjectType, mFlatType, mFlatSft, mRoomType, mRoomMeasurement;
    List<String> projectTypeList;
    List<String> flatTypeList;
    List<String> roomTypeList;
    //Firebase Database references
    DatabaseReference myRef;
    FirebaseDatabase database;

    private Dialog awesomeProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_room_details);
        btSubmit = findViewById(R.id.bt_submit);
        mProjectName = findViewById(R.id.project_name_et);
        mProjectType = findViewById(R.id.project_type_et);
        mFlatType = findViewById(R.id.flat_type_et);
        mFlatSft = findViewById(R.id.flat_sft_et);
        mRoomMeasurement = findViewById(R.id.room_measurement_et);
        mRoomType = findViewById(R.id.room_type_et);


        mProjectTypePickerUi = findViewById(R.id.project_type_picker_ui_view);
        mFlatTypePickerUi = findViewById(R.id.flat_type_picker_ui_view);
        mRoomTypePickerUi = findViewById(R.id.room_type_picker_ui_view);

        projectTypeList = Arrays.asList(getResources().getStringArray(R.array.project_type_list));
        flatTypeList = Arrays.asList(getResources().getStringArray(R.array.flat_type_list));
        roomTypeList = Arrays.asList(getResources().getStringArray(R.array.product_category_list));

        mProjectType.setOnClickListener(v -> {

            mProjectTypePickerUi.setVisibility(View.VISIBLE);
            PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                    .withItems(projectTypeList)
                    .withAutoDismiss(true)
                    .withItemsClickables(false)
                    .withUseBlur(false)
                    .build();

            mProjectTypePickerUi.setSettings(pickerUISettings);
            mProjectTypePickerUi.slide();
        });


        mFlatType.setOnClickListener(v->{

            mFlatTypePickerUi.setVisibility(View.VISIBLE);
            PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                    .withItems(flatTypeList)
                    .withAutoDismiss(true)
                    .withItemsClickables(false)
                    .withUseBlur(false)
                    .build();

            mFlatTypePickerUi.setSettings(pickerUISettings);
            mFlatTypePickerUi.slide();
        });

       mRoomType.setOnClickListener(v->{

           mRoomTypePickerUi.setVisibility(View.VISIBLE);
           PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                   .withItems(roomTypeList)
                   .withAutoDismiss(true)
                   .withItemsClickables(false)
                   .withUseBlur(false)
                   .build();

           mRoomTypePickerUi.setSettings(pickerUISettings);
           mRoomTypePickerUi.slide();
       });

        mProjectTypePickerUi.setOnClickItemPickerUIListener((which, position, valueResult) -> {
            Toast.makeText(AddRoomDetailsActivity.this, valueResult, Toast.LENGTH_SHORT).show();
            mProjectType.setText(valueResult);
            mProjectType.setError(null);
        });

        mFlatTypePickerUi.setOnClickItemPickerUIListener((which, position, valueResult) -> {
            Toast.makeText(AddRoomDetailsActivity.this, valueResult, Toast.LENGTH_SHORT).show();
            mFlatType.setText(valueResult);
            mFlatType.setError(null);
        });

        mRoomTypePickerUi.setOnClickItemPickerUIListener((which, position, valueResult) -> {
            Toast.makeText(AddRoomDetailsActivity.this, valueResult, Toast.LENGTH_SHORT).show();
            mRoomType.setText(valueResult);
            mRoomType.setError(null);
        });

        btSubmit.setOnClickListener(view -> {

            if(validateFields()) {
                String mProjectId = UUID.randomUUID().toString();

                myRef = fireBaseInitialSetup();

                final RoomDetails roomDetails = new RoomDetails();
                roomDetails.setmProjectName(mProjectName.getText().toString());
                roomDetails.setmProjectType(mProjectType.getText().toString());
                roomDetails.setmFlatType(mFlatType.getText().toString());
                roomDetails.setmFlatSft(mFlatSft.getText().toString());
                roomDetails.setmRoomType(mRoomType.getText().toString());
                roomDetails.setmRoomMeasurement(mRoomMeasurement.getText().toString());


                try {
                    awesomeProgressDialog = new AwesomeProgressDialog(AddRoomDetailsActivity.this).setMessage(R.string.adding_product).show();

                    myRef.child(CommonData.PROJECTS).child(mProjectId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (dataSnapshot.exists()) {

                                myRef.child(mProjectId).setValue(roomDetails).addOnCompleteListener(task -> {

                                    if (task.isSuccessful()) {
                                        // write code for successfull operation
                                        awesomeProgressDialog.dismiss();
                                        // Toast.makeText(AddExpensesActivity.this, "Bill Receipt for "+sharedPreferences.getString(CommonData.DISPLAY_NAME,"") + " is stored successfully", Toast.LENGTH_SHORT).show();
                                        Utils.showSuccessDialogue(AddRoomDetailsActivity.this, "Project " + mProjectName.getText().toString() + " is added successfully", AddInventoryActivity.class, true);
                                        Intent intent=new Intent();
                                        setResult(1,intent);
                                        finish();//finishing activity

                                    }
                                });
                            } else {
                                myRef.child(mProjectId).setValue(roomDetails).addOnCompleteListener(task -> {

                                    if (task.isSuccessful()) {
                                        // write code for successfull operation
                                        awesomeProgressDialog.dismiss();
                                        // Toast.makeText(AddExpensesActivity.this, "Bill Receipt for "+sharedPreferences.getString(CommonData.DISPLAY_NAME,"") + " is stored successfully", Toast.LENGTH_SHORT).show();
                                        Utils.showSuccessDialogue(AddRoomDetailsActivity.this, "Project " + mProjectName.getText().toString() + " is added successfully", AddInventoryActivity.class, true);
                                        Intent intent=new Intent();
                                        setResult(1,intent);
                                        finish();//finishing activity

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            awesomeProgressDialog.dismiss();
                            Toast.makeText(AddRoomDetailsActivity.this, "Something went wrong. Please try again..!!", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent();
                            setResult(1,intent);
                            finish();//finishing activity
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    awesomeProgressDialog.dismiss();
                    Toast.makeText(AddRoomDetailsActivity.this, "Unable to process request. Please try again..!!", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent();
                    setResult(1,intent);
                    finish();//finishing activity
                }

            }

        });


    }

    private DatabaseReference fireBaseInitialSetup() {

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference(CommonData.PROJECTS);
        return myRef;
    }

    private boolean validateFields() {

        if (mProjectName.getText().toString().trim().length() == 0) {
            setFocus(mProjectName, getResources().getString(R.string.enter_project_name));
            return false;
        }
        if (mProjectType.getText().toString().trim().length() == 0) {
            setFocus(mProjectType, getResources().getString(R.string.enter_project_type));
            return false;
        }
        if (mFlatType.getText().toString().trim().length() == 0) {
            setFocus(mFlatType, getResources().getString(R.string.select_flat_type));
            return false;
        }if (mFlatSft.getText().toString().trim().length() == 0) {
            setFocus(mFlatSft, getResources().getString(R.string.enter_flat_sft));
            return false;
        }if (mRoomType.getText().toString().trim().length() == 0) {
            setFocus(mRoomType, getResources().getString(R.string.select_room_type));
            return false;
        }
        if (mRoomMeasurement.getText().toString().trim().length() == 0) {
            setFocus(mRoomMeasurement, getResources().getString(R.string.enter_room_measurement));
            return false;
        }
        return true;
    }

    private void setFocus(TextInputEditText editText, String string) {
        editText.requestFocus();
        (editText).setError(Html.fromHtml("<font color='red'>" + string + "</font>"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(awesomeProgressDialog!=null)
        awesomeProgressDialog.dismiss();
    }
}
