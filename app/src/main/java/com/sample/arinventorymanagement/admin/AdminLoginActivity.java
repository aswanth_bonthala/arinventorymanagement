package com.sample.arinventorymanagement.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.sample.arinventorymanagement.R;

public class AdminLoginActivity extends AppCompatActivity {
    TextInputEditText mUserId, mPassword;
    Button mAdminLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);
        mUserId = findViewById(R.id.user_id_et);
        mPassword = findViewById(R.id.password_et);
        mAdminLogin = findViewById(R.id.admin_login_btn);

        mAdminLogin.setOnClickListener(v -> {

            if (validateFields()) {

                if (mUserId.getText().toString().equals("admin")
                        && mPassword.getText().toString().equals("admin")) {
                    startActivity(new Intent(AdminLoginActivity.this,AddInventoryActivity.class));
                    finish();
                } else {
                    Toast.makeText(AdminLoginActivity.this, getResources().getString(R.string.correct_details), Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    private boolean validateFields() {
        boolean valid = true;

        if (mUserId.getText().toString().trim().length() == 0) {
            setFocus(mUserId, "Please enter username");
            valid = false;
        } else if (mUserId.getText().toString().trim().length() < 5) {
            setFocus(mUserId, "Please enter valid username");
            valid = false;
        } else if (mPassword.getText().toString().trim().length() == 0) {
            setFocus(mPassword, "Please enter password");
            valid = false;
        } else if (mPassword.getText().toString().trim().length() < 5) {
            setFocus(mPassword, "Please enter valid password");
            valid = false;
        }
        return valid;
    }

    private void setFocus(TextInputEditText editText, String string) {
        editText.requestFocus();
        editText.setError(Html.fromHtml("<font color='red'>" + string + "</font>"));
    }


}
