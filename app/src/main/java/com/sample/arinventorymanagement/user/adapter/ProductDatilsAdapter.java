package com.sample.arinventorymanagement.user.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.arinventorymanagement.LaunchARActivity;
import com.sample.arinventorymanagement.R;
import com.sample.arinventorymanagement.admin.model.ProductDetails;
import com.sample.arinventorymanagement.user.model.RoomDetails;

import java.util.ArrayList;
import java.util.Locale;

public class ProductDatilsAdapter extends RecyclerView.Adapter<ProductDatilsAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ProductDetails> productDetailsArrayList;
    private ArrayList<ProductDetails> filterList = new ArrayList<>();


    public ProductDatilsAdapter(Context context, ArrayList<ProductDetails> productDetailsArrayList) {
        this.context = context;
        this.productDetailsArrayList = productDetailsArrayList;
        this.filterList.addAll(this.productDetailsArrayList);
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_details, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.productName.setText(filterList.get(position).getmName());
        holder.productType.setText("("+filterList.get(position).getmType()+")");
        holder.productBrand.setText(filterList.get(position).getmBrand());
        holder.productCategory.setText(filterList.get(position).getmCategory());
        holder.productPrice.setText("Price: "+ filterList.get(position).getmPrice());
        holder.productSize.setText("Dimensions: "+ filterList.get(position).getmSize());
        holder.productDescription.setText(filterList.get(position).getmDescription());

        holder.launchAr.setOnClickListener(v -> {
            try {
                Intent intent = new Intent(context, LaunchARActivity.class);
                intent.putExtra("productCategory",filterList.get(position).getmCategory());
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != filterList ? filterList.size() : 0);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productName;
        TextView productType;
        TextView productBrand;
        TextView productCategory;
        TextView productSize;
        TextView productPrice;
        TextView productDescription;
        Button launchAr;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.tv_product_name);
            productType = itemView.findViewById(R.id.tv_product_type);
            productBrand = itemView.findViewById(R.id.tv_product_brand);
            productCategory = itemView.findViewById(R.id.tv_product_category);
            productPrice = itemView.findViewById(R.id.tv_price);
            productSize = itemView.findViewById(R.id.tv_size);
            productDescription = itemView.findViewById(R.id.tv_product_desc);

            launchAr = itemView.findViewById(R.id.bt_launch_ar);
        }
    }

    // Do Search...
    public void filter(final String text) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {

                // Clear the filter list
                filterList.clear();

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {

                    filterList.addAll(productDetailsArrayList);

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (ProductDetails productDetails : productDetailsArrayList) {
                        if (productDetails.getmCategory().toLowerCase(Locale.getDefault()).contains(text.toLowerCase())) {
                            // Adding Matched items
                            filterList.add(productDetails);
                        }
                    }
                }

                // Set on UI Thread
                ((Activity) context).runOnUiThread(() -> {
                    // Notify the List that the DataSet has changed...
                    notifyDataSetChanged();
                });

            }
        }).start();

    }
}
