package com.sample.arinventorymanagement.admin.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductDetails implements Parcelable {

    private String mName;
    private String mBrand;
    private String mType;
    private String mCategory;
    private String mSize;
    private String mPrice;
    private String mDescription;
    private String mId;

    protected ProductDetails(Parcel in) {
        mName = in.readString();
        mBrand = in.readString();
        mType = in.readString();
        mCategory = in.readString();
        mSize = in.readString();
        mPrice = in.readString();
        mDescription = in.readString();
        mId = in.readString();
    }

    public ProductDetails() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mBrand);
        dest.writeString(mType);
        dest.writeString(mCategory);
        dest.writeString(mSize);
        dest.writeString(mPrice);
        dest.writeString(mDescription);
        dest.writeString(mId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductDetails> CREATOR = new Creator<ProductDetails>() {
        @Override
        public ProductDetails createFromParcel(Parcel in) {
            return new ProductDetails(in);
        }

        @Override
        public ProductDetails[] newArray(int size) {
            return new ProductDetails[size];
        }
    };

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmBrand() {
        return mBrand;
    }

    public void setmBrand(String mBrand) {
        this.mBrand = mBrand;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmCategory() {
        return mCategory;
    }

    public void setmCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public String getmSize() {
        return mSize;
    }

    public void setmSize(String mSize) {
        this.mSize = mSize;
    }

    public String getmPrice() {
        return mPrice;
    }

    public void setmPrice(String mPrice) {
        this.mPrice = mPrice;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }
}
