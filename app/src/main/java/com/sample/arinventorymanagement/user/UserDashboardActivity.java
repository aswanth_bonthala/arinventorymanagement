package com.sample.arinventorymanagement.user;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.chip.ChipGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sample.arinventorymanagement.CommonData;
import com.sample.arinventorymanagement.R;
import com.sample.arinventorymanagement.custom.AwesomeProgressDialog;
import com.sample.arinventorymanagement.user.adapter.ProjectDatilsAdapter;
import com.sample.arinventorymanagement.user.model.RoomDetails;

import java.util.ArrayList;

public class UserDashboardActivity extends AppCompatActivity {

    RecyclerView roomsList;
    TextView tvNoProjects;
    FloatingActionButton fabAddRoom;
    ChipGroup filterChipGroupSingleSelection;
    //Firebase Database references
    DatabaseReference myRef;
    FirebaseDatabase database;
    private Dialog awesomeProgressDialog;
    ArrayList<RoomDetails> roomDetailsArrayList = new ArrayList<RoomDetails>();
    ProjectDatilsAdapter projectDatilsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dashboard);
        roomsList = findViewById(R.id.rooms_list);
        tvNoProjects = findViewById(R.id.tv_no_projects_available);
        fabAddRoom = findViewById(R.id.fab_add_room);
        filterChipGroupSingleSelection = findViewById(R.id.choice_room_type_group);
        myRef = fireBaseInitialSetup();
        fetchProjectsDetails();
        filterChipGroupSingleSelection.setOnCheckedChangeListener((chipGroup, checkedId) -> {

            // Handle the checked chip change.
            switch (filterChipGroupSingleSelection.getCheckedChipId()) {
                case R.id.choice_living_room:
                    Toast.makeText(this, "Living Room", Toast.LENGTH_SHORT).show();
                    if (projectDatilsAdapter != null) {
                        projectDatilsAdapter.filter("Living Room");
                        projectDatilsAdapter.notifyDataSetChanged();
                    }
                    break;
                case R.id.choice_kitchen:
                    Toast.makeText(this, "Kitchen", Toast.LENGTH_SHORT).show();
                    if (projectDatilsAdapter != null) {
                        projectDatilsAdapter.filter("Kitchen");
                        projectDatilsAdapter.notifyDataSetChanged();
                    }
                    break;

                case R.id.choice_dining_room:
                    Toast.makeText(this, "Dining room", Toast.LENGTH_SHORT).show();
                    if (projectDatilsAdapter != null) {
                        projectDatilsAdapter.filter("Dining room");
                        projectDatilsAdapter.notifyDataSetChanged();
                    }
                    break;
                case R.id.choice_child_room:
                    Toast.makeText(this, "Children's room", Toast.LENGTH_SHORT).show();
                    if (projectDatilsAdapter != null) {
                        projectDatilsAdapter.filter("Children's room");
                        projectDatilsAdapter.notifyDataSetChanged();
                    }
                    break;
                case R.id.choice_bedroom:
                    Toast.makeText(this, "Bedroom", Toast.LENGTH_SHORT).show();
                    if (projectDatilsAdapter != null) {
                        projectDatilsAdapter.filter("Bedroom");
                        projectDatilsAdapter.notifyDataSetChanged();
                    }
                    break;
                case R.id.choice_bathroom:
                    Toast.makeText(this, "Bathroom", Toast.LENGTH_SHORT).show();
                    if (projectDatilsAdapter != null) {
                        projectDatilsAdapter.filter("Bathroom");
                        projectDatilsAdapter.notifyDataSetChanged();
                    }
                    break;
                default:
                    if (projectDatilsAdapter != null) {
                        projectDatilsAdapter.filter("");
                        projectDatilsAdapter.notifyDataSetChanged();
                    }
                    break;
            }
        });

        fabAddRoom.setOnClickListener(view -> {
            startActivityForResult(new Intent(UserDashboardActivity.this, AddRoomDetailsActivity.class), 1);
        });


    }

    private void fetchProjectsDetails() {
        awesomeProgressDialog = new AwesomeProgressDialog(UserDashboardActivity.this).setMessage(R.string.load_details).show();
        roomDetailsArrayList.clear();
        try {
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (dataSnapshot.exists()) {
                        awesomeProgressDialog.dismiss();
                        tvNoProjects.setVisibility(View.GONE);
                        roomsList.setVisibility(View.VISIBLE);
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            RoomDetails roomDetails = dataSnapshot1.getValue(RoomDetails.class);
                            roomDetailsArrayList.add(roomDetails);
                        }
                        projectDatilsAdapter = new ProjectDatilsAdapter(UserDashboardActivity.this, roomDetailsArrayList);
                        awesomeProgressDialog.dismiss();
                        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(UserDashboardActivity.this);
                        roomsList.setLayoutManager(linearLayoutManager);
                        roomsList.setItemAnimator(new DefaultItemAnimator());
                        roomsList.setAdapter(projectDatilsAdapter);
                        projectDatilsAdapter.notifyDataSetChanged();

                    } else {
                        tvNoProjects.setVisibility(View.VISIBLE);
                        roomsList.setVisibility(View.GONE);
                        awesomeProgressDialog.dismiss();
                        Toast.makeText(UserDashboardActivity.this, "Project Room details were not found", Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    awesomeProgressDialog.dismiss();
                    tvNoProjects.setVisibility(View.VISIBLE);
                    roomsList.setVisibility(View.GONE);
                    Toast.makeText(UserDashboardActivity.this, "No Results Found", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            awesomeProgressDialog.dismiss();
            tvNoProjects.setVisibility(View.VISIBLE);
            roomsList.setVisibility(View.GONE);
            Toast.makeText(UserDashboardActivity.this, "Unable to process request. Please try again..!!", Toast.LENGTH_SHORT).show();
        }
    }

    private DatabaseReference fireBaseInitialSetup() {

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference(CommonData.PROJECTS);
        return myRef;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 1
        if (requestCode == 1) {
            fetchProjectsDetails();
        }
    }
}
