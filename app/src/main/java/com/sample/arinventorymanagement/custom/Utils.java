package com.sample.arinventorymanagement.custom;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.sample.arinventorymanagement.R;


public class Utils {
	/**
	 * Check whether the internet connection is present or not. <uses-permission
	 * android:name="android.permission.ACCESS_NETWORK_STATE" />
	 */
	// To check whether network connection is available on device or not
	public static boolean checkInternetConnection(Activity _activity) {
		ConnectivityManager conMgr = (ConnectivityManager) _activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (conMgr.getActiveNetworkInfo() != null
				&& conMgr.getActiveNetworkInfo().isAvailable()
				&& conMgr.getActiveNetworkInfo().isConnected())
			return true;
		else
			return false;
	}
	
	/**
	 * Show an alert dialog and navigate back to previous screen if goBack is
	 * true
	 */
	public static void showAlertOkCancel(final Activity _activity,
                                         String title, String alertMsg, final boolean goBack) {
		AlertDialog.Builder alert = new AlertDialog.Builder(_activity);
		alert.setTitle(title);
		alert.setCancelable(false);
		alert.setMessage(alertMsg);
		alert.setPositiveButton(_activity.getResources().getString(R.string.dialog_ok_button), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (goBack)
					_activity.finish();
			}
		});
		alert.setNegativeButton(_activity.getResources().getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		alert.show();
	}

	//Warning alerts
	public static void showWarningDialogue(final Activity _activity, String title, String message, boolean setCancelable, final Class cls)
	{
		new AwesomeWarningDialog(_activity)
				.setTitle(R.string.app_name)
				.setMessage(message)
				.setCancelable(setCancelable)
				.setButtonTextColor(R.color.white)
				.setButtonText(_activity.getResources().getString(R.string.dialog_ok_button))
				.setWarningButtonClick(new Closure() {
					@Override
					public void exec() {
						// click
						Intent intent = new Intent(_activity,cls);
						_activity.startActivity(intent);
					}
				})
				.show();
	}

	public static void showWarningDialogue(final Activity _activity, String title, String message, boolean setCancelable, Class cls,
                                           String Date, String totalAmount, String merchantName)
	{
		new AwesomeWarningDialog(_activity)
				.setTitle(R.string.app_name)
				.setMessage(message)
				.setCancelable(setCancelable)
				.setButtonTextColor(R.color.white)
				.setButtonText(_activity.getResources().getString(R.string.dialog_ok_button))
				.setWarningButtonClick(new Closure() {
					@Override
					public void exec() {
						// click

					}
				})
				.show();
	}

	//Error alerts
	public static void showErrorDialogue(Activity _activity, String message)
	{
		new AwesomeErrorDialog(_activity)
				.setTitle(R.string.app_name)
				.setMessage(message)
				.setCancelable(true)
				.setButtonTextColor(R.color.white)
				.setButtonText(_activity.getResources().getString(R.string.dialog_ok_button))
				.setErrorButtonClick(new Closure() {
					@Override
					public void exec() {
						// click
					}
				})
				.show();
	}


	public static void showSuccessDialogue(final Activity _activity, String message, final Class cls, final Boolean isActivityClose)
    {
        new AwesomeSuccessDialog(_activity)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonText(_activity.getString(R.string.dialog_ok_button))
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        // click
						if (isActivityClose) {
							_activity.startActivity(new Intent(_activity,cls));
							_activity.finish();
						} else {
							_activity.startActivity(new Intent(_activity,cls));
						}

                    }
                })
                .show();
    }

	public static void showSuccessDialogue(final Activity _activity, String message, final Boolean isActivityClose)
	{
		new AwesomeSuccessDialog(_activity)
				.setTitle(R.string.app_name)
				.setMessage(message)
				.setCancelable(true)
				.setPositiveButtonTextColor(R.color.white)
				.setPositiveButtonText(_activity.getString(R.string.dialog_ok_button))
				.setPositiveButtonClick(new Closure() {
					@Override
					public void exec() {
						// click
						if (isActivityClose) {
							_activity.finish();
						}
					}
				})
				.show();
	}

    public static void showSuccessDialogue(final Activity _activity, String message)
    {
        new AwesomeSuccessDialog(_activity)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonText(_activity.getString(R.string.dialog_ok_button))
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        // click
                    }
                })
                .show();
    }


	public static String EncodeString(String string) {
		return string.replace(".", ",");
	}

	public static String DecodeString(String string) {
		return string.replace(",", ".");
	}
}
